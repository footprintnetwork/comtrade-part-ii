# README #

### What is this repository for? ###

* This repository holds all of the scripts that one needs to complete the data processing for the comtrade table in the comtrade download schema. 

### Order and execution of scripts: ###

1. new\_comtrade\_dc.R
	- This script replaces all of those listed below by performing all of their functions together.
	- Find and replace the comtrade\_18 schema name
	- If there is no matlab datacleaned data table with which to ensemble, then comment out that section (near the end of the script).
	- There is a manual datacleaning portion of the script that may need additions with each new release of comtrade.

1. Comtrade\_penult\_cleaned.R
	- This identifies, flags, and replaces outliers in the dataset and performs operations necessary for the next scripts to operate correctly. Adds a new flag column.
	- Replace comtrade\_\*\*\_sql table name.
1.  manual\_data\_cleaner.sql
	- This script holds a list of specific country-commodity-year changes that address recurring anomalies in the raw data. Creates 'MDC' flag.
	- Replace relevant table/schema names.
1. DatacleanerIndex.sql
	- This script creates tables to hold indices for back- and front-filling of missing observations. index\_maker\_world left blank until next script.
	- Replace relevant schema/table names. Change the year at the end of the insert query to the year after the most recent data in the set.
1. comtrade\_dc.R
	- This script does the linear interpolation between real observations and back- and front-filling from observations whose timelines don't extend to the beginning/end.
	- Replace relevant table/schema names (don't forget country\_matrix\__year_). Change the year at the end of the insert query for index\_maker\_world to the year after the most recent data year.
1. comtrade\_ensemble.sql
	- This script:
		- (If necessary) averages the output of comtrade\_dc.R with that of a matlab datacleaner, which may not be available, for select countries.
		- Aggregates countries into 'China' before putting them into the 'hk\_etc' schema.
		- Note: aggregation part assumes that the '\_ensemble' table has been made.
	- Replace relevant schemas/tables.

### Who do I talk to? ###

* Evan Neill
* David Lin
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)