/* 

QA COMTRADE after running the datacleaner (r)

some typical QA queries for COMTRADE when comparing between any two versions or editions
and at the end of this file, commented out, are country/year-specific queries

ultimately would be good to generate all of these as views in the ULT of a comtrade schema
	by a script that is run after successfully uploading all the data and applying datacleaner
    or in workbench - for now, load this file in workbench and can always find/replace:
    
this is:	comtrade_20_v2_
other is:	comtrade_20_v1_

as noted, some queries require _this_ to be in nfa2020 and first half of Q1 requires _other_ as well

*/

# Q1: this works in any NFA edition:
#
# are there any different commodities / commoditycodes? will return no rows if there are none
#	if there are differences, should look into this to see if this warrants template change
#
SELECT "in ULT schema" as `schema`, this.commodity_code, this.commodity_description, other.commodity_description
FROM `comtrade_20_v2_ult`.`commodity_key` as `this`
LEFT JOIN `comtrade_20_v1_ult`.`commodity_key` as `other`
USING (`commodity_code`)
WHERE this.commodity_description <> other.commodity_description;


# Q2: works to compare amongst any editions
#
# how many records there are pre vs post data cleaning
#
SELECT "# records SQL.comtrade_penult before data cleaning" as `info`,
(SELECT FORMAT((0+COUNT(*)),0) FROM `comtrade_20_v2_sql`.`comtrade_penult`) as `this`,
(SELECT FORMAT((0+COUNT(*)),0) FROM `comtrade_20_v1_sql`.`comtrade_penult`) as `other`
UNION
SELECT "# records ULT.comtrade_cleaned_ult after data cleaning",
(SELECT FORMAT((0+COUNT(*)),0) FROM `comtrade_20_v2_ult`.`comtrade_cleaned_ult`),
(SELECT FORMAT((0+COUNT(*)),0) FROM `comtrade_20_v1_ult`.`comtrade_cleaned_ult`);

# Q3: this works in any NFA edition:
#
# how many records were changed by data cleaner across all countries and years
#
SELECT "in this comtrade_penult" as `info`, 
FORMAT(SUM(if(GFN_flag="",1,0)),0) as `Data points not changed by data cleaner`,
FORMAT(SUM(if(GFN_flag="BFIL",1,0)),0) as `Backfilled`,
FORMAT(SUM(if(GFN_flag="FFIL",1,0)),0) as `Frontfilled`,
FORMAT(SUM(if(GFN_flag="OUTR",1,0)),0) as `Outlier`,
FORMAT(SUM(if(GFN_flag="MIS",1,0)),0) as `Interpolated`,
FORMAT(SUM(if(GFN_flag="MDC",1,0)),0) as `Manual Data Cleaned`,
FORMAT(SUM(if(GFN_flag="CnAG",1,0)),0) as `ChinaAggregated`
from `comtrade_20_v2_ult`.`comtrade_cleaned_ult`
UNION
SELECT "in other comtrade_penult" as `info`, 
FORMAT(SUM(if(GFN_flag="",1,0)),0) as `Data points not changed by data cleaner`,
FORMAT(SUM(if(GFN_flag="BFIL",1,0)),0) as `Backfilled`,
FORMAT(SUM(if(GFN_flag="FFIL",1,0)),0) as `Frontfilled`,
FORMAT(SUM(if(GFN_flag="OUTR",1,0)),0) as `Outlier`,
FORMAT(SUM(if(GFN_flag="MIS",1,0)),0) as `Interpolated`,
FORMAT(SUM(if(GFN_flag="MDC",1,0)),0) as `Manual Data Cleaned`,
FORMAT(SUM(if(GFN_flag="CnAG",1,0)),0) as `ChinaAggregated`
from `comtrade_20_v1_ult`.`comtrade_cleaned_ult`;

# Q4: 
#
# how many datapoints were or were not changed by country (across all years)
#	in excel you could analyse this further as % of datapoints that were changed vs kept
#	to help understand countries with more vs less dirty/cleaned data
#	also: could use this query to restrict to one country but display results by year to diagnose 
#
SELECT `country_code`, SUM(if(GFN_flag="",1,0)) as `Unchanged datapoints`, 
SUM(if(GFN_flag="BFIL",1,0)) as `Backfilled`, 
SUM(if(GFN_flag="FFIL",1,0)) as `Frontfilled`, 
SUM(if(GFN_flag="OUTR",1,0)) as `Outlier`, 
SUM(if(GFN_flag="MIS",1,0)) as `Interpolated`, 
SUM(if(GFN_flag="MDC",1,0)) as `Manual Data Cleaned`, 
SUM(if(GFN_flag="CnAG",1,0)) as `ChinaAggregated`
from `comtrade_20_v2_ult`.`comtrade_cleaned_ult`
#where `country_code`=33
group by `country_code`;

/*

# variant of Q4 above in case it might be needed to restrict to one country:
#
# how many datapoints were or were not changed by country (across all years)
#	in excel you could analyse this further as % of datapoints that were changed vs kept
#	to help understand countries with more vs less dirty/cleaned data
#	also: could use this query to restrict to one country but display results by year to diagnose 
#
SELECT `country_code`, `year`, SUM(if(GFN_flag="",1,0)) as `Unchanged datapoints`, 
SUM(if(GFN_flag="BFIL",1,0)) as `Backfilled`, 
SUM(if(GFN_flag="FFIL",1,0)) as `Frontfilled`, 
SUM(if(GFN_flag="OUTR",1,0)) as `Outlier`, 
SUM(if(GFN_flag="MIS",1,0)) as `Interpolated`, 
SUM(if(GFN_flag="MDC",1,0)) as `Manual Data Cleaned`, 
SUM(if(GFN_flag="CnAG",1,0)) as `ChinaAggregated`
from `comtrade_20_v2_ult`.`comtrade_cleaned_ult`
where `country_code`=33
group by `year`;

*/


# Q5: this works as long as _this_ is a schema in NFA2020 (but other can be in NFA2019):
#
# number records with different netweight_kg by year in the updatedtradedata
# expect the numbers to trend larger over time as more recent data tends to be changed more
#
SELECT this.year as `data year`, count(*) as `number netweight_kg data points that differ between this and other`
FROM `comtrade_20_v2_ult`.`comtrade_cleaned_ult` as `this`
LEFT JOIN `comtrade_20_v1_ult`.`comtrade_cleaned_ult` as `other`
USING (`year`, `Trade_flow`, `country_code`, `commodity_code`)
where (this.`year` >= (SELECT MIN(`year`) FROM `comtrade_20_v2_raw`.`comtrade_updatedtrade_data` LIMIT 1))
	and (this.netweight_kg <> other.netweight_kg)
group by `year`;


# Q6: this works as long as _this_ is a schema in NFA2020 (but other can be in NFA2019):
#
# number of netweight_kg differences, by country, when comparing this schema to last
# so can help to see what countries associated with most differences in data
#
SELECT this.country_code, cd.GFN_name, count(*) as `number netweight_kg data points that differ between this and other` #, `country_matrix_2020`.*
FROM `country_data`.`country_matrix_2020` as `cd`
LEFT JOIN `comtrade_20_v2_ult`.`comtrade_cleaned_ult` as `this`
	USING (`country_code`)
LEFT JOIN `comtrade_20_v1_ult`.`comtrade_cleaned_ult` as `other`
	USING (`year`, `Trade_flow`, `country_code`, `commodity_code`)
where (this.`year` >= (SELECT MIN(`year`) FROM `comtrade_20_v2_raw`.`comtrade_updatedtrade_data` LIMIT 1))
	and (this.netweight_kg <> other.netweight_kg)
group by `this`.`country_code`;


/*

# if needed, a template of queries to investigate specific country/year/commodity issues
# these are data-intensive queries so should restrict it by country code or a year
#	so replace ???? if/as appropriate


# Q8: this works in any NFA edition:
#
# in just one version/edition: this works back from ULT to SQL to see which trade records have changed the most
# 	for a specific year or country or commodity if/as needed
#
SELECT this.year, 
this.country_code, 
this.commodity_code, 
this.Trade_flow, 
this.GFN_flag as `GFN_flag in ULT`,
other.flag as `sql flag`, 
this.estimation_code as `comtrade flag`,
this.netweight_kg as `ULT netweight_kg`, 
other.netweight_kg as `SQL netweight_kg`, 
round((abs(ifnull(this.netweight_kg,0) - ifnull(other.netweight_kg,0))),0) as `difference rounded`
# ,this.*, other.* # if helpful to show all columns from both tables
FROM `comtrade_20_v2_ult`.`comtrade_cleaned_ult` as `this`
LEFT JOIN `comtrade_20_v2_sql`.`comtrade_penult` as `other`
USING (`year`, `Trade_flow`, `country_code`, `commodity_code`)
where this.`year`=???? and this.country_code=???? and this.commodity_code=????
# e.g. where this.`year`=2010 and this.country_code=33 and this.commodity_code=2432
order by `difference rounded` desc;

*/