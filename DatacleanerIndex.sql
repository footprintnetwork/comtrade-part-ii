#################################################
# DATA CLEANER PROJECT: INDEX MAKER
# This code creates an index for commodity trades from Comtrade, Fishstat(FAO), and Tradestat(FAO)
#	This must be run before running datacleaner.region
# comtrade_18_v2_sql <- use the sql schema
# tables needed 
#	`country_data`.`country_matrix_2018`  #UPDATE
#	`comtrade_18_v2_sql`.`comtrade_penult_cleaned_mdc` #UPDATE
#
# tables created:
#	`comtrade_18_v2_sql`.`index_maker_world` #UPDATE
#	`comtrade_18_v2_sql`.`index_maker_region` #UPDATE
#
# Update: the latest year should be updated in where clause.
#
#################################################

#############
# COMTRADE
#############

USE comtrade_18_v2_sql\p;
# create index_maker world
DROP TABLE IF EXISTS index_maker_world\p;
CREATE TABLE index_maker_world 
( year INT,
  trade_flow varchar(10) NOT NULL,
  UN_region varchar(100) NOT NULL,
  commodity_code int NOT NULL,
  next_netweight numeric(20,2),
  netweight numeric(20,2),
  growth_index double,
  FFIL_index double,
  CONSTRAINT PK_pr PRIMARY KEY (year,trade_flow,commodity_code,UN_region)
)\p;


# create index_maker_region
DROP TABLE IF EXISTS index_maker_region\p;
CREATE TABLE index_maker_region 
( year INT,
  trade_flow varchar(10) NOT NULL,
  UN_region varchar(100) NOT NULL,
  commodity_code int NOT NULL,
  next_netweight numeric(20,2),
  netweight numeric(20,2),
  growth_index double,
  FFIL_index double,
  CONSTRAINT PK_pr PRIMARY KEY (year,trade_flow,commodity_code,UN_region)
)\p;

# Regional ALL trade_flow
# creates the regional index for backfilling (it is called 'growth_index') and regional index for frontfilling ('FFIL_index')
INSERT INTO comtrade_18_v2_sql.index_maker_region
SELECT ft.*,ft.netweight/ft.next_netweight AS growth_index, ft.next_netweight/ft.netweight AS FFIL_index
FROM 
	(SELECT wow.year, wow.trade_flow,wow.UN_region,wow.commodity_code, @r AS `next_netweight`, (@r := value) AS `netweight`
	FROM (
		SELECT c.*
		FROM 
		(SELECT @code = NULL,
			@region = NULL,
            @flow = NULL
			) vars,
		(SELECT a.year,a.trade_flow,c.UN_region,a.commodity_code, SUM(a.netweight_kg) AS value
		FROM comtrade_18_v2_sql.comtrade_penult_cleaned_mdc a 
		LEFT JOIN country_data.country_matrix_2018 c
		ON a.country_code = c.country_code
		GROUP BY UN_region,year,trade_flow,commodity_code
		ORDER BY UN_region,commodity_code,trade_flow, year DESC) c) wow
	WHERE (CASE WHEN  (@code <> commodity_code AND @region <> UN_region AND @flow <> trade_flow) THEN 
			(@r := NULL) ELSE NULL END IS NULL) AND
			(@code := commodity_code AND @region := UN_region AND @flow := trade_flow) IS NOT NULL) ft
WHERE year <2017																								# UPDATE
ORDER BY UN_region,commodity_code,trade_flow,year DESC\p;


