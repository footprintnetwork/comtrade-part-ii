########################################################################
# 
#
#Added additional code for MDC items in NFA 2016 not cleaned in 2017 (presumably cleaned after this script was run last time)
# For NFA 2018 this script was not checked; the specific fixes at the commodity level were not validated. -EN 9/26/2017 
#
#		OUTPUT TABLES;
#		comtrade_18_v2_sql.comtrade_penult_cleaned_mdc #UPDATE FIRST
#		comtrade_18_v2_sql.manual_dc_17 # UPDATE FIRST
#
#		INPUT TABLES;
#		comtrade_18_v2_sql.comtrade_penult_cleaned #UPDATE NEXT
#		comtrade_17_v1_sql.comtrade_penult_cleaned_mdc # UPDATE NEXT
#		
#
#		FLAGS;
#		MDC - Manually data cleaneded
#		MVAL - 
#		OUT - Outlier
#
# I updated comtrade_16_v3_sql.manual_dc_15 to comtrade_17_v1_sql.manual_dc_16 -EN 9/26/2017
# I updated comtrade_17_v1_sql.manual_dc_16 to comtrade_18_v2_sql.manual_dc_17 -EN 9/26/2017
# I updated comtrade_16_v3_sql.comtrade_penult_cleaned_mdc to comtrade_17_v1_sql.comtrade_penult_cleaned_mdc -EN 9/26/2017
########################################################################
DROP TABLE IF EXISTS comtrade_18_v2_sql.manual_dc_17;
CREATE TABLE comtrade_18_v2_sql.manual_dc_17 AS(
SELECT year,trade_flow,country_code,commodity_code,trade_value,b.netweight_kg,'MDC' as flag,estimation_code,trade_quantity,unit_id 
FROM comtrade_18_v2_sql.comtrade_penult_cleaned a
JOIN (	SELECT year, trade_flow, country_code, cast(commodity_code AS DECIMAL) as commodity_code, netweight_kg
		FROM comtrade_17_v1_sql.manual_dc_16 
        WHERE flag = 'MVAL') b USING(year,trade_flow,country_code,commodity_code)
WHERE a.flag != 'OUT');

INSERT INTO comtrade_18_v2_sql.manual_dc_17
SELECT year, trade_flow, country_code, commodity_code, trade_value, 
trade_value/(	SELECT sum(trade_value)/sum(netweight_kg) as price FROM comtrade_18_v2_sql.comtrade_penult_cleaned 
				WHERE country_code = 56  AND trade_flow = 'Export' AND commodity_code = 8617 AND year != 2011
                AND year between 2001 AND 2013 AND trade_value IS NOT NULL AND netweight_kg IS NOT NULL),
'MDC', estimation_code,trade_quantity, unit_id FROM comtrade_18_v2_sql.comtrade_penult_cleaned
WHERE country_code = 56  AND trade_flow = 'Export' AND commodity_code = 8617 AND year = 2011;

INSERT INTO comtrade_18_v2_sql.manual_dc_17
SELECT year, trade_flow, country_code, commodity_code, trade_value, 
trade_value/(	SELECT sum(trade_value)/sum(netweight_kg) as price FROM comtrade_18_v2_sql.comtrade_penult_cleaned 
				WHERE country_code = 216  AND trade_flow = 'Export' AND commodity_code = 2433 AND year != 1999 
                AND year between 1994 AND 2004 AND trade_value IS NOT NULL AND netweight_kg IS NOT NULL),
'MDC', estimation_code,trade_quantity, unit_id FROM comtrade_18_v2_sql.comtrade_penult_cleaned
WHERE country_code = 216  AND trade_flow = 'Export' AND commodity_code = 2433 AND year = 1999;

INSERT INTO comtrade_18_v2_sql.manual_dc_17
SELECT year, trade_flow, country_code, commodity_code, trade_value, 
trade_value/(	SELECT sum(trade_value)/sum(netweight_kg) as price FROM comtrade_18_v2_sql.comtrade_penult_cleaned 
				WHERE country_code = 58  AND trade_flow = 'Import' AND commodity_code = 7321 AND year NOT IN (2001,2002)
				AND trade_value IS NOT NULL AND netweight_kg  IS NOT NULL),
'MDC', estimation_code,trade_quantity, unit_id FROM comtrade_18_v2_sql.comtrade_penult_cleaned
WHERE country_code = 58  AND trade_flow = 'Import' AND commodity_code = 7321 AND year in (2001,2002);

INSERT INTO comtrade_18_v2_sql.manual_dc_17
SELECT year, trade_flow, country_code, commodity_code, trade_value, 
trade_value/(	SELECT sum(trade_value)/sum(netweight_kg) as price FROM comtrade_18_v2_sql.comtrade_penult_cleaned 
				WHERE country_code = 69  AND trade_flow = 'Import' AND commodity_code = 7250 AND year != 1992
                AND trade_value IS NOT NULL AND netweight_kg  IS NOT NULL),
'MDC', estimation_code,trade_quantity, unit_id FROM comtrade_18_v2_sql.comtrade_penult_cleaned
WHERE country_code = 69  AND trade_flow = 'Import' AND commodity_code = 7250 AND year = 1992;

INSERT INTO comtrade_18_v2_sql.manual_dc_17
SELECT year, trade_flow, country_code, commodity_code, trade_value, 
trade_value/(	SELECT sum(trade_value)/sum(netweight_kg) as price FROM comtrade_18_v2_sql.comtrade_penult_cleaned 
				WHERE country_code = 18  AND trade_flow = 'Export' AND commodity_code = 752 AND year != 2008
                AND trade_value IS NOT NULL AND netweight_kg  IS NOT NULL),
'MDC', estimation_code,trade_quantity, unit_id FROM comtrade_18_v2_sql.comtrade_penult_cleaned
WHERE country_code = 18  AND trade_flow = 'Export' AND commodity_code = 752 AND year = 2008;

INSERT INTO comtrade_18_v2_sql.manual_dc_17
SELECT year, trade_flow, country_code, commodity_code, trade_value, 
trade_value/(	SELECT sum(trade_value)/sum(netweight_kg) as price FROM comtrade_18_v2_sql.comtrade_penult_cleaned 
				WHERE country_code = 221  AND trade_flow = 'Export' AND commodity_code = 6841 AND year != 2010
                AND trade_value IS NOT NULL AND netweight_kg  IS NOT NULL),
'MDC', estimation_code,trade_quantity, unit_id FROM comtrade_18_v2_sql.comtrade_penult_cleaned
WHERE country_code = 221  AND trade_flow = 'Export' AND commodity_code = 6841 AND year = 2010;

INSERT INTO comtrade_18_v2_sql.manual_dc_17
SELECT year, trade_flow, country_code, commodity_code, trade_value, 
trade_value/(	SELECT sum(trade_value)/sum(netweight_kg) as price FROM comtrade_18_v2_sql.comtrade_penult_cleaned 
				WHERE country_code = 50  AND trade_flow = 'Import' AND commodity_code = 3322 AND year != 1999
                AND trade_value IS NOT NULL AND netweight_kg  IS NOT NULL),
'MDC', estimation_code,trade_quantity, unit_id FROM comtrade_18_v2_sql.comtrade_penult_cleaned
WHERE country_code = 50  AND trade_flow = 'Import' AND commodity_code = 3322 AND year = 1999;

INSERT INTO comtrade_18_v2_sql.manual_dc_17
SELECT year, trade_flow, country_code, commodity_code, trade_value, 
trade_value/(	SELECT sum(trade_value)/sum(netweight_kg) as price FROM comtrade_18_v2_sql.comtrade_penult_cleaned 
				WHERE country_code = 108  AND trade_flow = 'Export' AND commodity_code = 3310 AND year NOT IN (1998,1999)
				 AND year between 1995 AND 2004 AND trade_value IS NOT NULL AND netweight_kg  IS NOT NULL),
'MDC', estimation_code,trade_quantity, unit_id FROM comtrade_18_v2_sql.comtrade_penult_cleaned
WHERE country_code = 108  AND trade_flow = 'Export' AND commodity_code = 3310 AND year in (1998,1999);

INSERT INTO comtrade_18_v2_sql.manual_dc_17
SELECT year, trade_flow, country_code, commodity_code, trade_value, 
trade_value/(	SELECT sum(trade_value)/sum(netweight_kg) as price FROM comtrade_18_v2_sql.comtrade_penult_cleaned 
				WHERE country_code = 118  AND trade_flow = 'Export' AND commodity_code = 3310 AND year NOT IN (1970,1971,1972,1973)
				 AND year between 1974 AND 1978 AND trade_value IS NOT NULL AND netweight_kg  IS NOT NULL),
'MDC', estimation_code,trade_quantity, unit_id FROM comtrade_18_v2_sql.comtrade_penult_cleaned
WHERE country_code = 118  AND trade_flow = 'Export' AND commodity_code = 3310 AND year in (1970,1971,1972,1973);

INSERT INTO comtrade_18_v2_sql.manual_dc_17
SELECT year, trade_flow, country_code, commodity_code, trade_value, 
trade_value/(	SELECT min(trade_value/netweight_kg) as price FROM comtrade_18_v2_sql.comtrade_penult_cleaned 
				WHERE trade_flow = 'Export' AND commodity_code = 3321 AND year = 2000),
'MDC', estimation_code,trade_quantity, unit_id FROM comtrade_18_v2_sql.comtrade_penult_cleaned
WHERE country_code = 134  AND trade_flow = 'Export' AND commodity_code = 3321 AND year = 2000;

INSERT INTO comtrade_18_v2_sql.manual_dc_17
SELECT year, trade_flow, country_code, commodity_code, trade_value, 
trade_value/(	SELECT sum(trade_value)/sum(netweight_kg) as price FROM comtrade_18_v2_sql.comtrade_penult_cleaned 
				WHERE country_code = 168  AND trade_flow = 'Export' AND commodity_code = 2831 AND year NOT IN (2004,2011)
				 AND year > 1990 AND trade_value IS NOT NULL AND netweight_kg  IS NOT NULL),
'MDC', estimation_code,trade_quantity, unit_id FROM comtrade_18_v2_sql.comtrade_penult_cleaned
WHERE country_code = 168  AND trade_flow = 'Export' AND commodity_code = 2831 AND year in (2004,2011);

INSERT INTO comtrade_18_v2_sql.manual_dc_17
SELECT year, trade_flow, country_code, commodity_code, trade_value, 
trade_value/(	SELECT sum(trade_value)/sum(netweight_kg) as price FROM comtrade_18_v2_sql.comtrade_penult_cleaned 
				WHERE country_code = 169  AND trade_flow = 'Import' AND commodity_code = 2762 AND year != 1996
                AND year between 1990 AND 2000 AND trade_value IS NOT NULL AND netweight_kg  IS NOT NULL),
'MDC', estimation_code,trade_quantity, unit_id FROM comtrade_18_v2_sql.comtrade_penult_cleaned
WHERE country_code = 169  AND trade_flow = 'Import' AND commodity_code = 2762 AND year = 1996;

INSERT INTO comtrade_18_v2_sql.manual_dc_17
SELECT year, trade_flow, country_code, commodity_code, trade_value, 
trade_value/(	SELECT sum(trade_value)/sum(netweight_kg) as price FROM comtrade_18_v2_sql.comtrade_penult_cleaned 
				WHERE country_code = 179  AND trade_flow = 'Export' AND commodity_code = 3321 AND year != 2003
                AND year > 1995 AND trade_value IS NOT NULL AND netweight_kg  IS NOT NULL),
'MDC', estimation_code,trade_quantity, unit_id FROM comtrade_18_v2_sql.comtrade_penult_cleaned
WHERE country_code = 179  AND trade_flow = 'Export' AND commodity_code = 3321 AND year = 2003;

INSERT INTO comtrade_18_v2_sql.manual_dc_17
SELECT year, trade_flow, country_code, commodity_code, trade_value, 
trade_value/(	SELECT sum(trade_value)/sum(netweight_kg) as price FROM comtrade_18_v2_sql.comtrade_penult_cleaned 
				WHERE country_code = 236  AND trade_flow = 'Export' AND commodity_code = 3321 AND year != 1964
                AND year < 1970 AND trade_value IS NOT NULL AND netweight_kg  IS NOT NULL),
'MDC', estimation_code,trade_quantity, unit_id FROM comtrade_18_v2_sql.comtrade_penult_cleaned
WHERE country_code = 236  AND trade_flow = 'Export' AND commodity_code = 3321 AND year = 1964;

DROP TABLE IF EXISTS comtrade_18_v2_sql.comtrade_penult_cleaned_mdc;
CREATE TABLE comtrade_18_v2_sql.comtrade_penult_cleaned_mdc LIKE comtrade_18_v2_sql.comtrade_penult_cleaned;
INSERT INTO comtrade_18_v2_sql.comtrade_penult_cleaned_mdc SELECT * FROM comtrade_18_v2_sql.comtrade_penult_cleaned;

UPDATE comtrade_18_v2_sql.comtrade_penult_cleaned_mdc a
JOIN comtrade_18_v2_sql.manual_dc_17 b USING(country_code, commodity_code, year, trade_value)
SET a.netweight_kg = b.netweight_kg, a.flag = b.flag;

## Update to copy actual "manual" data cleaned points from nfa 2018: comtrade_18_v2_sql.comtrade_penult_cleaned_mdc
## All Country/item/tradeflow timeseries corresponding to these "MDC" points have the same exact raw data values for netweight_kg.
## For select statement below, only update the comtrade_penult_cleaned_mdc table once per year and use the previous years table.

REPLACE INTO comtrade_18_v2_sql.comtrade_penult_cleaned_mdc
SELECT a.* 	FROM (select * from `comtrade_17_v1_sql`.`comtrade_penult_cleaned_mdc` where flag = 'MDC') a 
			LEFT JOIN comtrade_18_v2_sql.comtrade_penult_cleaned_mdc b 
			USING (trade_flow, country_code, commodity_code, year) 
			WHERE b.flag != 'MDC' \p;

/* Not used for NFA 2016
#Update UAE Comtrade values for petrol and gas trade for 2010, 2011, & 2012 with UAE sources values as saved in Y:\NFA 2015\4. Comparison.
REPLACE INTO `comtrade_17_baseline_sql`.`comtrade_penult_cleaned_mdc` VALUES 
(2010,'Import',225,3310,null,4146092,'UAE',null,4146092,1);
REPLACE INTO `comtrade_17_baseline_sql`.`comtrade_penult_cleaned_mdc`  VALUES 
(2011,'Import',225,3310,null,4146092,'UAE',null,4146092,1);
REPLACE INTO `comtrade_17_baseline_sql`.`comtrade_penult_cleaned_mdc` VALUES 
(2012,'Import',225,3310,null,4146092,'UAE',null,4146092,1);
REPLACE INTO `comtrade_17_baseline_sql`.`comtrade_penult_cleaned_mdc` VALUES 
(2010,'Export',225,3310,null,106527440000,'UAE',null,106527440000,1);
REPLACE INTO `comtrade_17_baseline_sql`.`comtrade_penult_cleaned_mdc` VALUES 
(2011,'Export',225,3310,null,121965480000,'UAE',null,121965480000,1);
REPLACE INTO `comtrade_17_baseline_sql`.`comtrade_penult_cleaned_mdc` VALUES 
(2012,'Export',225,3310,null,143510000000,'UAE',null,143510000000,1);

REPLACE INTO `comtrade_17_baseline_sql`.`comtrade_penult_cleaned_mdc` VALUES 
(2010,'Import',225,3411,null,19021700000,'UAE',null,19021700000,1);
REPLACE INTO `comtrade_17_baseline_sql`.`comtrade_penult_cleaned_mdc`  VALUES 
(2011,'Import',225,3411,null,20923500000,'UAE',null,20923500000,1);
REPLACE INTO `comtrade_17_baseline_sql`.`comtrade_penult_cleaned_mdc` VALUES 
(2012,'Import',225,3411,null,20079110000,'UAE',null,20079110000,1);
REPLACE INTO `comtrade_17_baseline_sql`.`comtrade_penult_cleaned_mdc` VALUES 
(2010,'Export',225,3411,null,11921400000,'UAE',null,11921400000,1);
REPLACE INTO `comtrade_17_baseline_sql`.`comtrade_penult_cleaned_mdc` VALUES 
(2011,'Export',225,3411,null,11669800000,'UAE',null,11669800000,1);
REPLACE INTO `comtrade_17_baseline_sql`.`comtrade_penult_cleaned_mdc` VALUES 
(2012,'Export',225,3411,null,11115740000,'UAE',null,11115740000,1);