##################################################################################################################################
########THIS IS A POST DATA CLEANER SCRIPT THAT FIXES COMTRADE DATA BASED ON COMMODITIES BY COUNTRY & YEAR THE RESEARCH TEAM IDENTIFIED AS CAUSING UNFAVORABLE 
#		RESULTS 
#
#	1. MatLab/R Ensemble Patch for ugly timelines 
# 	The new R-Datacleaner - which uses an index method to fill data, appears much more conservative across all countries - however there are a few countries where the 
#   index method was much more agressive, here we take the average of the netweight_kg values from the MatLab and R datacleaner output tables: 
#
#			`comtrade_18_v2_ult`.`comtrade_r_dt_ult`
#			`comtrade_18_v2_ult`.`comtrade_ult_newoutlier_mdc`	
#
#
#################################################################################################
#
#
#			INPUT TABLES	//tables that go injected into the script 
#			`comtrade_18_v2_ult`.`comtrade_r_dt_ult`			##UPDATE
#			`comtrade_18_v2_ult`.`comtrade_ult_newoutlier_mdc`		##UPDATE
#
#			OUTPUT TABLES // tables that are outputed by the script 
#			`comtrade_18_v2_ult`.`comtrade_ensemble`				##UPDATE  
#			
#
#			FLAGS
#			MDCF - MANUALLY DATA CLEANED FISHSTAT BY PART 2 OF THIS SCRIPT
#
#
#
#####################################################################################################################################
#1. Matlab & R Ensemble patch for ugly timelines

##Comtrade Ensemble
# Left join to MDC from last year since we don't have it this year.
# Since it goes through 2014 should have the same desired affect, for the most part.
USE `comtrade_18_v2_ult`;
DROP TABLE IF EXISTS `comtrade_ensemble`;
CREATE TABLE `comtrade_ensemble` like comtrade_r_dt_ult \p;
INSERT INTO `comtrade_ensemble` 
(SELECT  
                year,  
                trade_flow,   
                country_code,   
                commodity_code,   
                a.trade_value,  
                IF((country_code IN (12, 26,54, 123, 102, 162, 182, 7) AND a.GFN_flag IN ('BFIL', 'FFIL') and b.netweight_kg IS NOT NULL),(a.netweight_kg + b.netweight_kg)/2, a.netweight_kg),     
                a.estimation_code,  
                a.trade_quantity,  
                a.unit_id,  
                a.GFN_flag
FROM comtrade_18_v2_ult.comtrade_r_dt_ult a  
LEFT JOIN comtrade_17_v1_ult.comtrade_ult_newoutlier_mdc b   
USING(year, trade_flow, country_code, commodity_code)) \p;

##########################################################################
## China aggsplit part

INSERT INTO comtrade_18_v2_ult.comtrade_ensemble
SELECT year,trade_flow,351,commodity_code,sum(trade_value),sum(netweight_kg),Estimation_code,sum(trade_quantity),unit_id,'CnAG' 
FROM comtrade_18_v2_ult.comtrade_ensemble
WHERE country_code IN (41,96,214,128)
GROUP BY commodity_code, trade_flow, year\p;

CREATE SCHEMA IF NOT EXISTS comtrade_18_v2_ult_hketc\p;

USE comtrade_18_v2_ult_hketc;
DROP TABLE IF EXISTS comtrade_ensemble;
CREATE TABLE comtrade_ensemble like comtrade_18_v2_ult.comtrade_ensemble;
INSERT INTO comtrade_ensemble SELECT * FROM comtrade_18_v2_ult.comtrade_ensemble
WHERE country_code in (41, 96, 128, 214)\p;

DELETE FROM comtrade_18_v2_ult.comtrade_ensemble
WHERE country_code in (41, 96, 128, 214)\p;

###########################################################################################
# Create view for comtrade_ult
###########################################################################################

#1. 
	DROP VIEW IF EXISTS comtrade_18_v2_ult.comtrade_ensemble_desc;
	CREATE VIEW comtrade_18_v2_ult.comtrade_ensemble_desc AS
    SELECT 
		a.year,
		a.trade_flow,
		a.country_code,
		b.gfn_name as country,
		LPAD(CAST(a.commodity_code AS CHAR),4,0) AS commodity_code,
		c.commodity_description,
		a.trade_value,
		a.netweight_kg,
		a.estimation_code,
		a.trade_quantity,
		d.unit as trade_quantity_units,
        GFN_flag
    FROM comtrade_18_v2_ult.comtrade_ensemble a
    LEFT JOIN country_data.country_matrix_2018 b USING(country_code)
	LEFT JOIN comtrade_18_v2_sql.commodity_key c USING(commodity_code)
	LEFT JOIN comtrade_18_v2_sql.unit_key d ON a.Unit_id = d.id;
    
	DROP VIEW IF EXISTS comtrade_18_v2_ult_hketc.comtrade_ensemble_desc;
	CREATE VIEW comtrade_18_v2_ult_hketc.comtrade_ensemble_desc AS
    SELECT 
		a.year,
		a.trade_flow,
		a.country_code,
		b.gfn_name as country,
		LPAD(CAST(a.commodity_code AS CHAR),4,0) AS commodity_code,
		c.commodity_description,
		a.trade_value,
		a.netweight_kg,
		a.estimation_code,
		a.trade_quantity,
		d.unit as trade_quantity_units,
        GFN_flag
    FROM comtrade_18_v2_ult_hketc.comtrade_ensemble a
    LEFT JOIN country_data.country_matrix_2018 b USING(country_code)
	LEFT JOIN comtrade_18_v2_sql.commodity_key c USING(commodity_code)
	LEFT JOIN comtrade_18_v2_sql.unit_key d ON a.Unit_id = d.id;
